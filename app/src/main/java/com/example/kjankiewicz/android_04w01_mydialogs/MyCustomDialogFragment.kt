package com.example.kjankiewicz.android_04w01_mydialogs

import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.View
import android.widget.EditText
import android.widget.SeekBar

//import java.util.ArrayList
import java.util.Locale

class MyCustomDialogFragment : DialogFragment() {
    //private val mSelectedItems = ArrayList<Any>()

    override fun onCreateDialog(savedInstanceState: Bundle?): AlertDialog {
        val builder = AlertDialog.Builder(activity)
        val inflater = activity!!.layoutInflater
        val layout = inflater.inflate(R.layout.dialog_color, null)
        builder.setTitle(R.string.custom_dialog_title)
                .setIcon(R.drawable.ic_rgb)
                .setView(layout) // variant 3
                //.setMessage(R.string.custom_dialog_message) // variant 1
                /*.setItems(R.array.kolory_array) { _, which ->
                    // variant 2a
                    mSelectedItems.clear()
                    mSelectedItems.add(which)
                }*/
                /*.setSingleChoiceItems( // variant 2b
                        R.array.kolory_array, -1) { _, which ->
                    mSelectedItems.clear();
                    mSelectedItems.add(which);
                }*/
                /*.setMultiChoiceItems( // variant 2c
                        R.array.kolory_array,
                        null) { _, which, isChecked ->
                    if (isChecked) {
                        mSelectedItems.add(which)
                    } else if (mSelectedItems.contains(which)) {
                        mSelectedItems.remove(Integer.valueOf(which))
                    }
                }*/
                .setNegativeButton(R.string.no) { _, _ ->
                    // nic nie robimy
                }
                .setPositiveButton(R.string.yes) { _, _ ->
                    (activity as MainActivity).setPositiveColor(
                            (layout.findViewById<View>(R.id.czerwonySeekBar) as SeekBar).progress,
                            (layout.findViewById<View>(R.id.zielonySeekBar) as SeekBar).progress,
                            (layout.findViewById<View>(R.id.niebieskiSeekBar) as SeekBar).progress
                    )
                }
                .setNeutralButton(R.string.cancel) { _, _ ->
                    // nic nie robimy
                }

        class MyOnSeekBarChangeListener(val myEditText: EditText) : SeekBar.OnSeekBarChangeListener {

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                //add code here
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                //add code here
            }

            override fun onProgressChanged(seekBark: SeekBar, progress: Int, fromUser: Boolean) {
                myEditText.setText(String.format(Locale.ENGLISH, "%d", progress))
            }
        }

        val czerwonySeekBar = layout.findViewById<SeekBar>(R.id.czerwonySeekBar)
        val zielonySeekBar = layout.findViewById<SeekBar>(R.id.zielonySeekBar)
        val niebieskiSeekBar = layout.findViewById<SeekBar>(R.id.niebieskiSeekBar)

        czerwonySeekBar.setOnSeekBarChangeListener(MyOnSeekBarChangeListener(layout.findViewById(R.id.czerwonyEditText)))
        zielonySeekBar.setOnSeekBarChangeListener(MyOnSeekBarChangeListener(layout.findViewById(R.id.zielonyEditText)))
        niebieskiSeekBar.setOnSeekBarChangeListener(MyOnSeekBarChangeListener(layout.findViewById(R.id.niebieskiEditText)))

        return builder.create()
    }
}
