package com.example.kjankiewicz.android_04w01_mydialogs

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import android.text.format.DateFormat
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.DatePicker
import android.widget.TimePicker

import java.util.Calendar


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var button = findViewById<Button>(R.id.zamknij_app_button)
        button.setOnClickListener {
            val newFragment = CzyZamknacDialogFragment()
            newFragment.show(supportFragmentManager, "zamknac")
        }
        button = findViewById(R.id.ustaw_date_button)
        button.setOnClickListener {
            val datePickerFragment = DatePickerFragment()
            datePickerFragment.show(supportFragmentManager, "ustaw_date")
        }

        button = findViewById(R.id.ustaw_godzine_button)
        button.setOnClickListener {
            val timePickerFragment = TimePickerFragment()
            timePickerFragment.show(supportFragmentManager, "ustaw_godzine")
        }

        button = findViewById(R.id.custom_dialog_button)
        button.setOnClickListener {
            val myCustomDialogFragment = MyCustomDialogFragment()
            myCustomDialogFragment.show(supportFragmentManager, "custom_dialog")
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        return id == R.id.action_settings || super.onOptionsItemSelected(item)
    }

    class CzyZamknacDialogFragment : DialogFragment() {
        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

            val builder = AlertDialog.Builder(activity)
            builder.setMessage(R.string.czy_chcesz_zamknac_aplikacje)
                    .setPositiveButton(R.string.yes) { _, _ -> activity!!.finish() }
                    .setNegativeButton(R.string.cancel) { _, _ ->
                        // nic nie robimy
                    }
            return builder.create()
        }
    }

    class DatePickerFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            return DatePickerDialog(context!!, this, year, month, day)
        }

        override fun onDateSet(view: DatePicker, year: Int, month: Int, day: Int) {
            // coś można z tą datą zrobić
        }
    }

    class TimePickerFragment : DialogFragment(), TimePickerDialog.OnTimeSetListener {

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val c = Calendar.getInstance()
            val hour = c.get(Calendar.HOUR_OF_DAY)
            val minute = c.get(Calendar.MINUTE)

            return TimePickerDialog(activity, this, hour, minute,
                    DateFormat.is24HourFormat(activity))
        }

        override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
            // coś można z tym czasem zrobić
        }
    }

    fun setPositiveColor(vr: Int, vg: Int, vb: Int) {
        val rv: View = findViewById(android.R.id.content)
        val newColor = Color.rgb(vr, vg, vb)
        rv.setBackgroundColor(newColor)
    }

}
